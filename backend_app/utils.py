import io
import base64
import numpy as np
from os import listdir
from os.path import join
from PIL import Image, ImageFilter, ImageEnhance


def load_seed(seed_path):
    return np.load(seed_path).astype(np.float32)


def load_mask(mask_path):
    return np.array(Image.open(mask_path))


def load_painting(painting_path):
    with open(painting_path, 'rb') as image_file:
        return base64.b64encode(image_file.read()).decode('utf-8')


def load_thumbnails(thumbnails_path):
    thumbnails_list = []

    for image_name in listdir(thumbnails_path):
        image_path = join(thumbnails_path, image_name)
        image_name_without_extension = image_name.split('.')[0]
        image_name_tokens = image_name_without_extension.split('_')
        image_index = int(image_name_tokens[0])
        pattern_name = '_'.join(image_name_tokens[1:])

        with open(image_path, 'rb') as image_file:
            image = base64.b64encode(image_file.read()).decode('utf-8')
        thumbnails_list.append((image_index, pattern_name, image))

    return sorted(thumbnails_list, key=lambda x: x[0])


def load_model_set(gem_path):
    seed_path = join(gem_path, 'reconstructors', 'painting_reconstructor.npy')
    mask_path = join(gem_path, 'painting_set', 'label.png')
    thumbnails_path = join(gem_path, 'painting_set', 'thumbnails')

    seed = load_seed(seed_path)
    mask = load_mask(mask_path)
    thumbnails = load_thumbnails(thumbnails_path)

    return mask, seed, thumbnails


def numpy_to_base64(numpy_image):
    pil_image = Image.fromarray(numpy_image)
    return pil_to_base64(pil_image)


def pil_to_base64(pil_image):
    buf = io.BytesIO()
    pil_image.save(buf, format='png')
    _bytes = buf.getvalue()

    return base64.b64encode(_bytes).decode('utf-8')


def base64_to_numpy(base64_image, image_type):
    decoded_bytes = base64.b64decode(base64_image.encode('utf-8'))
    return bytes_to_numpy(decoded_bytes, image_type)


def bytes_to_numpy(blob_image, image_type):
    assert image_type in ('grayscale', 'rgb')
    conversion_format = 'RGB' if image_type == 'rgb' else 'L'
    image = Image.open(io.BytesIO(blob_image)).convert(conversion_format)

    return np.array(image)


def create_delta(image_shape):
    empty_image = np.zeros(image_shape, dtype=np.uint8)
    return numpy_to_base64(empty_image)


def compute_new_mask(mask, brush_delta, brush_pattern):
    mask[brush_delta > 0] = brush_pattern
    return mask


def compute_new_seed(seed, brush_delta, seed_mean, seed_std):
    delta_seed = np.random.normal(size=brush_delta.shape, loc=seed_mean, scale=seed_std)
    new_seed = np.copy(seed)
    new_seed[brush_delta > 0] = delta_seed[brush_delta > 0]
    return new_seed


def postprocess_model_output(output):
    scaled_image = np.clip((output + 1) * 127.5, 0, 255).astype(np.uint8)

    pil_image = Image.fromarray(scaled_image)
    pil_image = pil_image.filter(ImageFilter.SMOOTH)

    enhancer = ImageEnhance.Contrast(pil_image)
    pil_image = enhancer.enhance(1.2)

    return pil_to_base64(pil_image)
