import json
from os.path import join, basename
from decouple import config as env_config
from django import forms
from django.shortcuts import redirect
from django.middleware.csrf import get_token
from django.http import JsonResponse, HttpResponse
from django.views.decorators.http import require_POST, require_GET

from backend_app.utils import (
    create_delta,
    load_painting,
    load_model_set,
    bytes_to_numpy,
    compute_new_mask,
    compute_new_seed,
)

if env_config('MODEL_PREDICTION') == 'LOCAL':
    from backend_app.predictors.local import get_model_prediction, load_model

    model = None
elif env_config('MODEL_PREDICTION') == 'CLOUD':
    from backend_app.predictors.cloud import get_model_prediction
else:
    raise ValueError('MODEL_PREDICTION env var must be set to either LOCAL or CLOUD')


def index(request):
    return HttpResponse("Hello, world. You're at the backend_app index.")


@require_GET
def get_csrf(request):
    return JsonResponse({'csrfToken': get_token(request)})


@require_GET
def get_available_models(request):
    models_dict = {}
    with open(join('backend_app', 'gems_lookup.json'), 'r') as json_fp:
        gem_path_lookup = json.load(json_fp)

    for painting_index, gem_path in gem_path_lookup.items():
        painting_path = join(gem_path, 'painting_set', 'painting.png')
        models_dict[int(painting_index)] = load_painting(painting_path)

    return HttpResponse(json.dumps(models_dict), content_type='application/json')


class RepainterForm(forms.Form):
    seedStd = forms.FloatField(label='seedStd')
    seedMean = forms.FloatField(label='seedMean')
    brushDelta = forms.FileField(label='brushDelta')
    brushPattern = forms.IntegerField(label='brushPattern')


@require_POST
def process_painting(request):
    mask = request.session['mask']
    seed = request.session['seed']
    num_styles = request.session['num_styles']

    if env_config('MODEL_PREDICTION') == 'LOCAL':
        global model
        if model is None:
            return HttpResponse(
                f'The model was not initialized properly',
                content_type='text/plain',
                status=500,
            )
    else:
        model = request.session['model_name']

    form = RepainterForm(request.POST, request.FILES)
    if form.is_valid():
        seed_std = 0.5 * 10 ** form.cleaned_data['seedStd']
        seed_mean = form.cleaned_data['seedMean']
        brush_pattern = form.cleaned_data['brushPattern']
        brush_delta_bytes = form.cleaned_data['brushDelta'].read()
        brush_delta = bytes_to_numpy(brush_delta_bytes, 'grayscale')

        mask = compute_new_mask(mask, brush_delta, brush_pattern)
        seed = compute_new_seed(seed, brush_delta, seed_mean, seed_std)
        repainting = get_model_prediction(model, num_styles, mask, seed)

        request.session['mask'] = mask
        request.session['seed'] = seed

        response_dict = dict(painting=repainting)
        return HttpResponse(json.dumps(response_dict), content_type='application/json')

    return HttpResponse('/process_painting received an invalid request form', status=500)


@require_GET
def select_painting(request):
    with open(join('backend_app', 'gems_lookup.json'), 'r') as json_fp:
        gem_path_lookup = json.load(json_fp)

    painting_index = str(request.GET.get('painting_index'))
    gem_path = gem_path_lookup[painting_index]
    mask, seed, thumbnails = load_model_set(gem_path)
    delta = create_delta(mask.shape)
    num_styles = len(thumbnails)

    model_name = basename(gem_path)
    if env_config('MODEL_PREDICTION') == 'LOCAL':
        global model
        model_path = join(gem_path, 'saved_model', model_name)
        model = load_model(model_path)
    else:
        model = model_name
        request.session['model_name'] = model_name

    request.session['mask'] = mask
    request.session['seed'] = seed
    request.session['num_styles'] = num_styles

    repainting = get_model_prediction(model, num_styles, mask, seed)
    response_dict = dict(
        image_shape=mask.shape,
        delta=delta,
        painting=repainting,
        thumbnails=thumbnails,
    )
    return HttpResponse(json.dumps(response_dict), content_type='application/json')


@require_GET
def initialize_painting(request):
    return redirect('/backend_app/select_painting?painting_index=0')
