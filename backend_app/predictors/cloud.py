import numpy as np
from os.path import join
from os import environ
from json_tricks import dumps
from decouple import config as env_config
from googleapiclient import discovery
from google.api_core.client_options import ClientOptions

from backend_app.utils import postprocess_model_output

BYTES_ENCODING = 'ISO-8859–1'


def get_model_prediction(model_name, num_styles, mask, seed):
    seed = np.around(seed, 2)
    seed_bytes = dumps(seed, compression=True, properties={'ndarray_compact': True})
    mask_bytes = dumps(mask, compression=True, properties={'ndarray_compact': True})

    instances = {
        'seed': seed_bytes.decode(encoding=BYTES_ENCODING),
        'mask': mask_bytes.decode(encoding=BYTES_ENCODING),
        'num_styles': num_styles,
    }

    endpoint = 'https://ml.googleapis.com'
    project_id = env_config('CLOUD_PROJECT_ID')
    model_version = env_config('CLOUD_MODEL_VERSION')

    client_options = ClientOptions(api_endpoint=endpoint)
    ml = discovery.build('ml', 'v1', client_options=client_options)

    request_body = {'instances': instances}
    request = ml.projects().predict(
        name=f'projects/{project_id}/models/{model_name}/versions/{model_version}',
        body=request_body,
    )
    response = request.execute()
    output = np.asarray(response['predictions'])

    return postprocess_model_output(output)
