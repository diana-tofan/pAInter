import numpy as np
import tensorflow as tf

from backend_app.utils import postprocess_model_output


def load_model(model_path):
    return tf.saved_model.load(model_path)


def get_model_prediction(model, num_styles, mask, seed):
    seed = seed[np.newaxis, ..., np.newaxis]
    mask = mask[np.newaxis, ...]

    seed_tensor = tf.constant(seed, dtype=np.float32, name='seed')
    mask_tensor = tf.one_hot(mask, depth=num_styles, name='mask')

    output = model([seed_tensor, mask_tensor], False, None).numpy()[0]
    return postprocess_model_output(output)
