from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('get_csrf', views.get_csrf, name='get_csrf'),
    path('select_painting', views.select_painting, name='select_painting'),
    path('process_painting', views.process_painting, name='process_painting'),
    path('initialize_painting', views.initialize_painting, name='initialize_painting'),
    path('get_available_models', views.get_available_models, name='get_available_models'),
]
