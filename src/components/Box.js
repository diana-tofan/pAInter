import "../style/box.scss";

export const Box = ({ title, value, children, className }) => (
  <div class={`box ${className ? className : ""}`}>
    {title && (
      <div class="title">
        <span>{title}</span>
        <span class="value">{value}</span>
      </div>
    )}
    <div class="component">{children}</div>
  </div>
);
