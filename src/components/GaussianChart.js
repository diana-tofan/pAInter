import { useState, useEffect, useRef } from "preact/hooks";
import { Box } from "./Box";
import * as d3 from "d3";
import jStat from "jstat";

const GaussianChart = ({ seedMean, seedStd }) => {
  const margin = { top: 20, right: 20, bottom: 20, left: 20 };
  const width = 300;
  const height = 200;

  const interval = 0.005;
  const upperBound = 1.0;
  const lowerBound = -1.0;

  const [isSecondCurveDrawn, setSecondCurve] = useState(false);

  function createData(interval, upperBound, lowerBound, mean, std) {
    const n = Math.ceil((upperBound - lowerBound) / interval);
    const data = [];

    let x_position = lowerBound;
    for (let i = 0; i < n; i++) {
      data.push({
        y: jStat.normal.pdf(x_position, mean, std),
        x: x_position,
      });
      x_position += interval;
    }
    return data;
  }

  const svgRef = useRef();

  // will be called initially and on every data change
  useEffect(() => {
    const svg = d3.select(svgRef.current);
    svg.selectAll("path").remove();
    svg.selectAll(".tick").remove();

    const dataset = createData(
      interval,
      upperBound,
      lowerBound,
      seedMean,
      seedStd
    );

    const hardcodedDataset = createData(
      interval,
      upperBound,
      lowerBound,
      0,
      0.5
    );

    // define scales
    const xScale = d3
      .scaleLinear()
      .domain([
        d3.min(dataset, function (d) {
          return d.x;
        }),
        d3.max(dataset, function (d) {
          return d.x + 0.1;
        }),
      ])
      .range([0, width]);
    xScale.nice();

    const y0 = Math.max(
      Math.abs(
        d3.min(dataset, function (d) {
          return d.y;
        })
      ),
      Math.abs(
        d3.max(dataset, function (d) {
          return d.y < 1 ? 1 : d.y;
        })
      )
    );
    const yScale = d3.scaleLinear().domain([0, y0]).range([height, 0]);

    // define axis
    const xAxis = d3.axisBottom().scale(xScale).ticks(12);
    const yAxis = d3.axisLeft().scale(yScale).ticks(8);

    // create svg
    svg
      .attr("width", width + margin.left + margin.right)
      .attr("height", height + margin.top + margin.bottom)
      .append("g")
      .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    const line = d3
      .line()
      .x(function (d) {
        return xScale(d.x);
      })
      .y(function (d) {
        return yScale(d.y);
      });

    const gradPurple = svg
      .append("defs")
      .append("linearGradient")
      .attr("id", "gradPurple")
      .attr("x1", "0%")
      .attr("x2", "0%")
      .attr("y1", "0%")
      .attr("y2", "100%");

    gradPurple
      .append("stop")
      .attr("offset", "0%")
      .attr("stop-color", "#d2baff")
      .attr("stop-opacity", 0.6);
    gradPurple
      .append("stop")
      .attr("offset", "80%")
      .attr("stop-color", "white")
      .attr("stop-opacity", 0);
    svg
      .append("path")
      .datum(hardcodedDataset)
      .attr("transform", "translate(30,1)")
      .attr("class", "line line2")
      .attr("d", line)
      .attr("stroke-width", "2px")
      .style("stroke", "#d2baff")
      .style("fill", "url(#gradPurple)")
      .style("opacity", "1")
      .style("shape-rendering", "auto");

    const grad = svg
      .append("defs")
      .append("linearGradient")
      .attr("id", "grad")
      .attr("x1", "0%")
      .attr("x2", "0%")
      .attr("y1", "0%")
      .attr("y2", "100%");

    grad
      .append("stop")
      .attr("offset", "0%")
      .attr("stop-color", "#72e3af")
      .attr("stop-opacity", 0.6);
    grad
      .append("stop")
      .attr("offset", "80%")
      .attr("stop-color", "white")
      .attr("stop-opacity", 0);

    svg
      .append("path")
      .datum(dataset)
      .attr("transform", "translate(30,1)")
      .attr("class", "line line1")
      .attr("d", line)
      .attr("stroke-width", "2px")
      .style("stroke", "#72e3af")
      .style("fill", "url(#grad)")
      .style("opacity", "1")
      .style("shape-rendering", "auto");

    // append axes
    svg
      .append("g")
      .attr("class", "x axis")
      .attr("transform", `translate(30,${height + 1})`)
      .call(xAxis);

    svg
      .append("g")
      .attr("class", "y axis")
      .attr("transform", "translate(30,1)")
      .call(yAxis);

    const dur = 50;

    // update axis
    svg.select(".x.axis").transition().duration(dur).call(xAxis);

    svg.select(".y.axis").transition().duration(dur).call(yAxis);
  }, [seedMean, seedStd]);

  return <svg ref={svgRef} />;
};

export const renderGaussianChart = (title, seedMean, seedStd) => {
  const seedStdUpdated = 0.5 * 10 ** seedStd;
  return (
    <Box
      title={title}
      children={<GaussianChart seedMean={seedMean} seedStd={seedStdUpdated} />}
      className="gaussianChart"
    />
  );
};
