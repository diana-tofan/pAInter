import { Box } from "./Box";
import "../style/slider.scss";

const Slider = ({ value, onChange, min, max, step, title }) => {
  const setValue = () => {
    const range = document.getElementById(`range-${title}`);
    const value = ((range.value - range.min) / (range.max - range.min)) * 100;
    range.style.background = `linear-gradient(to right, #e2e98c 0%, #8ce9a6 ${
      value / 2
    }%, #8ce9d8 ${value}%, #25353e ${value}%, #25353e 100%)`;
  };
  return (
    <input
      type="range"
      id={`range-${title}`}
      min={min}
      max={max}
      step={step}
      value={value}
      oninput={(e) => {
        setValue();
        return onChange(e);
      }}
      class="rangeSlider"
    />
  );
};

export const renderSlider = (title, value, setValue, min, max, step) => (
  <Box
    title={title}
    value={value}
    children={
      <Slider
        value={value}
        onChange={(e) => setValue(e.target.value)}
        min={min}
        max={max}
        step={step}
        title={title}
      />
    }
    className={title.split(" ")[1]}
  />
);

export const renderSliderSeedStd = (title, value, setValue, min, max, step) => (
  <Box
    title={title}
    value={(0.5 * 10 ** value).toFixed(2)}
    children={
      <Slider
        value={value}
        onChange={(e) => setValue(e.target.value)}
        min={min}
        max={max}
        step={step}
        title={title}
      />
    }
    className={title.split(" ")[1]}
  />
);
