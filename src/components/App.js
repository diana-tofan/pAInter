import { render } from "preact";
import { Header } from "./Header.js";
import { View } from "./View";
import "../style/index.scss";

export const App = () =>
  <div class="app" id="app">
  <Header />
  <View />
</div>

render(<App />, document.getElementById('root'));
