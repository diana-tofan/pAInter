import { Box } from "./Box";

const Thumbnails = ({ list, selectImage, value }) => {
  return list.map((thumbnail, i) => (
    <img
      src={`data:image/png;base64, ${thumbnail[2]}`}
      alt="thumbnail"
      class={`thumbnailImage ${value === thumbnail[1] ? "selected" : ""}`}
      onClick={() => {
        selectImage(i);
      }}
      width={50}
      height={50}
    />
  ));
};

export const renderThumbnails = (
  title,
  value,
  setValue,
  setBrushIndex,
  thumbnails
) => (
  <Box
    title={title}
    value={value}
    children={
      <div class="thumbnailsContainer">
        <Thumbnails
          value={value}
          list={thumbnails}
          selectImage={(i) => {
            setValue(thumbnails[i][1]);
            setBrushIndex(i);
          }}
        />
      </div>
    }
    className="pattern"
  />
);
