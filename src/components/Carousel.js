import { Box } from "./Box";
import { arrowIcon } from "./Icons";
import { useState, useEffect } from "preact/hooks";
import "../style/carousel.scss";

const Carousel = ({ value, onChange, slides }) => {
  const [currentPosition, setCurrentPosition] = useState(0);
  const [slidesArray, setSlidesArray] = useState([]);

  useEffect(() => {
    setSlidesArray(Object.keys(slides));
  }, [slides]);

  const reorder = (data, index) =>
    data && data.slice(index).concat(data.slice(0, index));

  const arrowRightClick = () => {
    if (currentPosition !== Object.keys(slides).length - 1) {
      setCurrentPosition(currentPosition + 1);
      setSlidesArray(reorder(Object.keys(slides), currentPosition));
    }
  };

  const arrowLeftClick = () => {
    if (currentPosition !== 0) {
      setCurrentPosition(currentPosition - 1);
      setSlidesArray(reorder(Object.keys(slides), currentPosition));
    }
  };

  return (
    <div class="carouselWrapper">
      <img
        src={arrowIcon}
        class="leftArrow"
        alt="prev"
        width={30}
        height={30}
        onClick={arrowLeftClick}
      />
      {slidesArray &&
        slidesArray.map((key, i) => (
          <img
            src={`data:image/png;base64, ${slides[key]}`}
            alt="image"
            class={`image ${i > 3 ? "hidden" : "visible"} ${
              parseInt(value) === parseInt(key) ? "selected" : ""
            }`}
            onClick={() => {
              onChange(key);
            }}
            width={110}
            height={75}
          />
        ))}
      <img
        src={arrowIcon}
        alt="next"
        width={30}
        height={30}
        onClick={arrowRightClick}
      />
    </div>
  );
};

export const renderCarousel = (
  value,
  setValue,
  slides,
  initialPattern,
  setBrushPattern
) => (
  <Box
    value={value}
    children={
      <Carousel
        slides={slides}
        value={value}
        onChange={(image) => {
          setValue(image);
          setBrushPattern(initialPattern);
        }}
      />
    }
    className="selectedImage"
  />
);
