import { Box } from "./Box";

const ColorPicker = ({ value, onChange }) => (
  <div>
    <input
      type="color"
      name="colorPicker"
      value={value}
      class="colorPicker"
      onChange={onChange}
    />
    <label for="colorPicker"> change color</label>
  </div>
);

export const renderColorPicker = (title, value, setValue) => (
  <Box
    title={title}
    value={value}
    children={
      <ColorPicker value={value} onChange={(e) => setValue(e.target.value)} />
    }
    className="colorPickerBox"
  />
);
