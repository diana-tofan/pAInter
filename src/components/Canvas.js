import { useState, useEffect, useRef } from "preact/hooks";
import "../style/canvas.scss";
import { processPainting } from "../utils/requests";
import { renderSpinner } from "./Spinner";

export const Canvas = ({
  data,
  brushSize,
  brushColor,
  brushShape,
  brushIndex,
  seedMean,
  seedStd,
  loading,
  setLoading,
}) => {
  const [drawing, setDrawing] = useState(false);
  const [image, setImage] = useState(null);

  const [brushDelta, setBrushDelta] = useState(null);

  useEffect(() => {
    const image = createImage(data.painting);
    const brushDelta = createDelta(data.delta);
    setImage(image);
    setBrushDelta(brushDelta);
  }, [data]);

  const canvasRef = useRef();
  const ctx = useRef();

  const canvasBottomRef = useRef();
  const ctxCanvasBottom = useRef();

  useEffect(() => {
    ctx.current = canvasRef.current.getContext("2d");
    ctxCanvasBottom.current = canvasBottomRef.current.getContext("2d");
  }, []);

  function handleMouseMove(e) {
    // actual coordinates
    const coords = [
      e.clientX - canvasRef.current.offsetLeft,
      e.clientY - canvasRef.current.offsetTop,
    ];
    if (drawing) {
      ctx.current.lineTo(...coords);
      ctx.current.stroke();
      ctxCanvasBottom.current.lineTo(...coords);
      ctxCanvasBottom.current.stroke();
    }
  }

  function startDrawing(e) {
    [ctx, ctxCanvasBottom].forEach((ctx) => {
      ctx.current.lineJoin = brushShape;
      ctx.current.lineCap = brushShape;
      ctx.current.lineWidth = brushSize;
      ctx.current.strokeStyle = brushColor;
      ctx.current.globalAlpha = 0.05;
      ctx.current.beginPath();
      // actual coordinates
      ctx.current.moveTo(
        e.clientX - canvasRef.current.offsetLeft,
        e.clientY - canvasRef.current.offsetTop
      );
      setDrawing(true);
    });
  }

  async function stopDrawing() {
    ctx.current.closePath();
    ctxCanvasBottom.current.closePath();
    setDrawing(false);
    const blob = await new Promise((resolve) =>
      document.getElementById("canvasBottom").toBlob(resolve, "image/png")
    );
    const formData = new FormData();
    formData.append("brushPattern", brushIndex);
    formData.append("brushDelta", blob);
    formData.append("seedMean", seedMean);
    formData.append("seedStd", seedStd);
    const body = formData;
    ctxCanvasBottom.current.clearRect(
      0,
      0,
      canvasBottomRef.current.width,
      canvasBottomRef.current.height
    );
    setLoading(true);
    const outputImage = await processPainting(body);
    setLoading(false);
    createImage(outputImage.painting);
  }

  const createImage = (imgData) => {
    const canvas = document.getElementById("canvas");
    const canvasBottom = document.getElementById("canvasBottom");
    const ctx = canvas.getContext("2d");
    const image = new Image();
    image.onload = function () {
      [canvas, canvasBottom].forEach((canvas) => {
        canvas.width = image.width;
        canvas.style.width = `${image.width}px`;
        canvas.height = image.height;
        canvas.style.height = `${image.height}px`;
      });
      canvasBottom.style.height = canvas.style.height;
      canvasBottom.style.width = canvas.style.width;
      canvasBottom.style.background = "#000";
      ctx.drawImage(image, 0, 0);
    };

    image.src = "data:image/png;base64, " + imgData;

    return document.getElementById("canvas").toDataURL();
  };

  const createDelta = (brushDelta) => {
    const image = new Image();
    image.src = "data:image/png;base64, " + brushDelta;
    return image;
  };

  return (
    <div class="image-container">
      <canvas id="canvasBottom" ref={canvasBottomRef} />
      <canvas
        class={loading ? "loading" : ""}
        id="canvas"
        ref={canvasRef}
        onMouseDown={startDrawing}
        onMouseUp={stopDrawing}
        onMouseMove={handleMouseMove}
      />
      {loading && renderSpinner(60)}
    </div>
  );
};
