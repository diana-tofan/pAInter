import { useEffect, useState } from "preact/hooks";
import { Canvas } from "./Canvas";
import {
  getAvailableModels,
  initializePainting,
  selectPainting,
} from "../utils/requests";
import { renderSlider, renderSliderSeedStd } from "./Slider";
import { renderThumbnails } from "./Thumbnails";
import { renderColorPicker } from "./ColorPicker";
import { renderBrushShape } from "./BrushShape";
import { renderGaussianChart } from "./GaussianChart";
import { renderCarousel } from "./Carousel";
import { renderSpinner } from "./Spinner";

export const View = () => {
  const [data, setData] = useState(null);
  const [painting, setPainting] = useState(0);
  const [images, setImages] = useState([]);
  const [brushSize, setBrushSize] = useState(40);
  const [seedMean, setSeedMean] = useState(0);
  const [seedStd, setSeedStd] = useState(0);
  const [brushPattern, setBrushPattern] = useState("");
  const [brushIndex, setBrushIndex] = useState(0);
  const [brushColor, setBrushColor] = useState("#ffffff");
  const [brushShape, setBrushShape] = useState("round");
  const [loading, setLoading] = useState(false);

  useEffect(async () => {
    setLoading(true);
    const data = await selectPainting(painting);
    setData(data);
    setLoading(false);
  }, [painting]);

  useEffect(async () => {
    const paintings = await getAvailableModels();
    const data = await initializePainting();
    setData(data);
    setImages(paintings);
    setBrushPattern(data.thumbnails[0][1]);
  }, []);

  return (
    <div class="content">
      <div class="left">
        {renderSlider("brush size", brushSize, setBrushSize, 10, 80, 1)}
        {renderSlider("seed mean", seedMean, setSeedMean, -0.1, 0.1, 0.01)}
        {renderSliderSeedStd("seed std", seedStd, setSeedStd, -1, 1, 0.01)}
        {renderGaussianChart("input seed distribution", seedMean, seedStd)}
      </div>
      <div class="middle">
        {data ? (
          <Canvas
            data={data}
            brushSize={brushSize}
            brushColor={brushColor}
            brushShape={brushShape}
            brushPattern={brushPattern}
            brushIndex={brushIndex}
            seedMean={seedMean}
            seedStd={seedStd}
            loading={loading}
            setLoading={setLoading}
          />
        ) : (
          <div class="canvas-container ">{renderSpinner(60)}</div>
        )}
        {data ? (
          images &&
          renderCarousel(
            painting,
            setPainting,
            images,
            data.thumbnails[0][1],
            setBrushPattern
          )
        ) : (
          <div class="carousel-container">
            Setting up the app or switching images can take around 1 minute...<br/>
            Try refreshing if loading takes too long.
          </div>
        )}
      </div>
      <div class="right">
        {data &&
          renderThumbnails(
            "brush pattern",
            brushPattern,
            setBrushPattern,
            setBrushIndex,
            data.thumbnails
          )}
        {renderColorPicker("brush color", brushColor, setBrushColor)}
        {renderBrushShape("brush shape", brushShape, setBrushShape)}
      </div>
    </div>
  );
};
