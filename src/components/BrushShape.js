import { Box } from "./Box";

const BrushShape = ({ value, onChange }) => (
  <div class="shapesContainer">
    <div
      class={`round ${value === "round" ? "selected" : ""}`}
      onClick={() => onChange("round")}
    />
    <div
      class={`square ${value === "butt" ? "selected" : ""}`}
      onClick={() => onChange("butt")}
    />
  </div>
);

export const renderBrushShape = (title, value, setValue) => (
  <Box
    title={title}
    value={value}
    children={
      <BrushShape value={value} onChange={(shape) => setValue(shape)} />
    }
    className="brushShape"
  />
);
