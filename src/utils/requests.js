let _csrfToken = null;

const API_HOST = "/backend_app";

async function getCsrfToken() {
  if (_csrfToken === null) {
    const response = await fetch(`${API_HOST}/get_csrf`, {
      credentials: "include",
    });
    const data = await response.json();
    _csrfToken = data.csrfToken;
  }
  return _csrfToken;
}

export const getAvailableModels = async () => {
  try {
    const response = await fetch(`${API_HOST}/get_available_models`, {
      credentials: "include",
    });
    return await response.json();
  } catch (err) {
    throw new Error(err);
  }
};

export const initializePainting = async (id) => {
  try {
    const response = await fetch(`${API_HOST}/initialize_painting`, {
      credentials: "include",
    });
    return await response.json();
  } catch (err) {
    throw new Error(err);
  }
};

export const selectPainting = async (id) => {
  try {
    const response = await fetch(
      `${API_HOST}/select_painting?painting_index=${id}`,
      {
        credentials: "include",
      }
    );
    return await response.json();
  } catch (err) {
    throw new Error(err);
  }
};

export const processPainting = async (body) => {
  try {
    const response = await fetch(`${API_HOST}/process_painting`, {
      method: "POST",
      body,
      headers: {
        "X-CSRFToken": await getCsrfToken(),
        Accept: "application/json",
      },
      credentials: "include",
    });
    return await response.json();
  } catch (err) {
    throw new Error(err);
  }
};
